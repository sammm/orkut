package assignment2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Queue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import javax.annotation.Resource;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public class SparseMatrixCSC extends SparseMatrix implements Runnable {


    // TODO: variable declarations

    int[] index;

    int[] source;

    /**
     * Constructor.
     *
     *
     * @param file CSC file to read and parse.
     * @throws FileNotFoundException bad file spec.
     * @throws UnsupportedEncodingException
     * @throws Exception unexpected 'other'.
     */
    SparseMatrixCSC( String file ) {
        try {
            InputStreamReader is
                = new InputStreamReader( new FileInputStream( file ), "UTF-8" );
            BufferedReader rd = new BufferedReader( is );
            readFile( rd );
        } catch( FileNotFoundException e ) {
            System.err.println( "File not found: " + e );
            return;
        } catch( UnsupportedEncodingException e ) {
            System.err.println( "Unsupported encoding exception: " + e );
            return;
        } catch( Exception e ) {
            System.err.println( "Exception: " + e );
            return;
        }
    }

    void readFile( BufferedReader rd ) throws Exception {

        String line = rd.readLine();
        if( line == null ) {
            throw new Exception("premature end of file");
        }
        if( !line.equalsIgnoreCase( "CSC" ) && !line.equalsIgnoreCase( "CSC-CSR" ) ) {
            throw new Exception("file format error -- header");
        }
        num_vertices = getNext(rd);
        num_edges = getNext(rd);

        source = new int[num_edges]; //must be length m
        index = new int[num_vertices+1]; //must be length n+1

        int where = 0;
        for( int i=0; i < num_vertices; ++i ) {
            line = rd.readLine();
            if( line == null ) {
                throw new Exception("premature end of file");
            }
            String elm[] = line.split( " " );
            assert Integer.parseInt( elm[0] ) == i : "Error in CSC file";
            index[i] = where;
            for( int x=1; x < elm.length; ++x ) {
                int src = Integer.parseInt( elm[x] );
                source[where++] = src;
            }
        }
        assert where == num_edges : "Mismatch in number of edges in CSR file";
        index[num_vertices] = num_edges;

    }


    // Auxiliary function for PageRank calculation
    void calculateOutDegree( int outdeg[] ) {
        for( int v=0; v < num_edges; ++v ) {
            outdeg[source[v]] += 1;
        }
    }


    @Override
    public void run() {
    }

    /**
     *
     * Use threads to distribute the work in a Concurrent fashion.
     *
     * @param a
     * @param in
     * @param out
     * @param outdeg
     * @param startPos
     * @param endPos
     */
    void iterateConcurrent(double a, double[] in, double[] out, int outdeg[], int startPos, int endPos) {
        for( int v=startPos; v < endPos; ++v ) {
            int v1 = v;
            int estart = index[v];
            int eend = index[v+1];

            for( int e=estart; e < eend; ++e ) {
                double w = (a * in[source[e]] / (double)outdeg[source[e]]);
                out[v] += w;
            }

            /*
            Thread worker = new Thread () {
                @Override
                public void run() {
                    for( int e=estart; e < eend; ++e ) {
                        double w = (a * in[source[e]] / (double)outdeg[source[e]]);
                        out[v1] += w;
                    }
                }
            };
            worker.start();
            */

        }
    }


    /**
     *
     * @param a
     * @param in
     * @param out
     * @param outdeg
     */
    void iterate( double a, double[] in, double[] out, int outdeg[]) {
        for( int v=0; v < num_vertices; ++v ) {
            int estart = index[v];
            int eend = index[v+1];
            for( int e=estart; e < eend; ++e ) {
                double w = (a * in[source[e]] / (double)outdeg[source[e]]);
                out[v] += w;
            }
        }
    }

    private int getNext(BufferedReader rd) throws IOException {
        String line = rd.readLine();
        if (line == null || line.isEmpty()) {
            return 0;
        }
        return Integer.parseInt(line);
    }


}
