package assignment2;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Array;
import java.util.Queue;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Semaphore;
import javax.annotation.Resource;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.io.FileOutputStream;
import java.io.PrintWriter;

public abstract class SparseMatrix {

    int num_vertices; // Number of vertices in the graph

    int num_edges;    // Number of edges in the graph

    // Auxiliary in preparation of PageRank iteration: pre-calculate the
    // out-degree (number of outgoing edges) for each vertex
    abstract void calculateOutDegree( int outdeg[] );

    // Perform one PageRank iteration.
    //    a: damping factor
    //    in[]: previous PageRank values, read-only
    //    out[]: new PageRank values, initialised to zero
    //    outdeg[]: values pre-calculated by calculateOutDegree()
    //abstract void iterate( double a, double[] in, double[] out, int outdeg[], int startPos, int endPos );

    abstract void iterate( double a, double[] in, double[] out, int outdeg[]);

    abstract void iterateConcurrent( double a, double[] in, double[] out, int outdeg[], int startPos, int endPos );

}
