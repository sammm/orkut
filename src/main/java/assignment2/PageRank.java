package assignment2;

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

public class PageRank {

    //public static Semaphore sem = new Semaphore(0);
    public static Boolean flag = false;
    public static CyclicBarrier barrier = new CyclicBarrier(1);
    public static SparseMatrix matrix_c;
    //SparseMatrix matrix = new SparseMatrixCSC( args[2] );

    public static void main( String args[] ) {
        if( args.length < 5 ) {
            System.err.println( "Usage: java pagerank graph.coo graph.csr graph.csc threads outputfile" );
            return;
        }

        long tm_start = System.nanoTime();

        System.out.println( "COO: " + args[0] );
        System.out.println( "CSR: " + args[1] );
        System.out.println( "CSC: " + args[2] );

        // SparseMatrix matrix = new SparseMatrixCOO( args[0] );
        // SparseMatrix matrix = new SparseMatrixCSR( args[1] );

        SparseMatrix matrix = new SparseMatrixCSC( args[2] );
        matrix_c = matrix;

        int num_threads = Integer.parseInt( args[3] );
        System.out.println( "Number of threads: " + num_threads );

        String outputFile = args[4];

        long tm_end = System.nanoTime();
        double tm_input = (double)(tm_end - tm_start) * 1e-9;
        tm_start = tm_end;
        System.out.println( "Reading input: " + args[0] + " seconds" );
        final int n = matrix.num_vertices;
        double x[] = new double[n];
        double v[] = new double[n];
        double y[] = new double[n];
        final double d = 0.85;
        final double tol = 1e-7;
        final int max_iter = 100;
        final boolean verbose = true;
        double delta = 2;
        int iter = 0;

        for( int i=0; i < n; ++i ) {
            x[i] = v[i] = ((double)1) / (double)n;
            y[i] = 0;
        }

        int outdeg[] = new int[n];
        matrix.calculateOutDegree( outdeg );

        double tm_init = (double)(System.nanoTime() - tm_start) * 1e-9;
        System.err.println( "Initialisation: " + tm_init + " seconds" );
        tm_start = System.nanoTime();

        while( iter < max_iter && delta > tol ) {
            // Power iteration step.
            // 1. Transfering weight over out-going links (summation part)
            //matrix.iterate(d, x, y, outdeg);
            try {

                barrier.await();
                //mainThread.matrix = (SparseMatrixCSC)matrix;
                //otherThread.matrix = (SparseMatrixCSC)matrix;

                for (int id=0 ; id < num_threads; id++){
                    //matrix.iterate( d, x, y, outdeg, num_threads )
                    if (id == 0) {
                        //call main thread
                        //mainThread.iterateConcurrent(d, x, y, outdeg, iterateCalcIn(id, n, num_threads), iterateCalcOut(id, n, num_threads));
                        //mainThread.run();
                        final CSCThreadMain mainThread = new CSCThreadMain(/*args[2], matrix,*/ id, delta, tol, d, x, y, outdeg, n, num_threads);
                        mainThread.start();
                        //mainThread.run();
                    } else {
                        //call other theads
                        //matrix.iterateConcurrent(d, x, y, outdeg, iterateCalcIn(id, n, num_threads), iterateCalcOut(id, n, num_threads));
                        //otherThread.run();
                        final CSCThreadOther otherThread = new CSCThreadOther(/*args[2], matrix,*/ id, delta, tol, d, x, y, outdeg, n, num_threads);
                        otherThread.start();
                        //otherThread.run();
                    }


                    barrier.await();
                }
            } catch (InterruptedException ex) {
                System.out.println(ex);
            } catch (BrokenBarrierException ex) {
                System.out.println(ex);
            }

            // 2. Constants (1-d)v[i] added in separately.
            double w = 1.0 - sum( y, n ); // ensure y[] will sum to 1
            for( int i=0; i < n; ++i )
                y[i] += w * v[i];

            // Calculate residual error
            delta = normdiff( x, y, n );
            iter++;

            // Rescale to unit length and swap x[] and y[]
            w = 1.0 / sum( y, n );
            for( int i=0; i < n; ++i ) {
                x[i] = y[i] * w;
                y[i] = 0.;
            }

            double tm_step = (double)(System.nanoTime() - tm_start) * 1e-9;
            if( verbose )
                System.err.println( "iteration " + iter + ": delta=" + delta
                    + " xnorm=" + sum(x, n)
                    + " time=" + tm_step + " seconds" );
            tm_start = System.nanoTime();
        }

        if( delta > tol )
            System.err.println( "Error: solution has not converged." );

        // Dump PageRank values to file
        writeToFile( outputFile, x, n );
    }

    static double sum( double[] a, int n ) {
        double d = 0.;
        double err = 0.;
        for( int i=0; i < n; ++i ) {
            // The code below achieves
            // d += a[i];
            // but does so with high accuracy
            double tmp = d;
            double y = a[i] + err;
            d = tmp + y;
            err = tmp - d;
            err  += y;

        }
        return d;
    }

    static double normdiff( double[] a, double[] b, int n ) {
        double d = 0.;
        double err = 0.;
        for( int i=0; i < n; ++i ) {
            // The code below achieves
            // d += Math.abs(b[i] - a[i]);
            // but does so with high accuracy
            double tmp = d;
            double y = Math.abs( b[i] - a[i] ) + err;
            d = tmp + y;
            err = tmp - d;
            err += y;
        }
        return d;
    }

    static void writeToFile( String file, double[] v, int n ) {
        try {
            OutputStreamWriter os
                = new OutputStreamWriter( new FileOutputStream( file ), "UTF-8" );
            BufferedWriter wr = new BufferedWriter( os );
            writeToBuffer( wr, v, n );
        } catch( FileNotFoundException e ) {
            System.err.println( "File not found: " + e );
            return;
        } catch( UnsupportedEncodingException e ) {
            System.err.println( "Unsupported encoding exception: " + e );
            return;
        } catch( Exception e ) {
            System.err.println( "Exception: " + e );
            return;
        }
    }
    static void writeToBuffer( BufferedWriter buf, double[] v, int n ) {
        PrintWriter out = new PrintWriter( buf );
        for( int i=0; i < n; ++i )
            out.println( i + " " + v[i] );
        out.close();
    }

    static class CSCThreadMain extends Thread {

        int id;
        double tol;
        double delta;
        //Boolean flag = false;
        CyclicBarrier barrier = new CyclicBarrier(1);
        double w;
        int[] outdeg;
        double d;
        double x[];
        double y[];
        int num_threads;
        int num_vertices;
        //PageRank pr;
        //SparseMatrixCSC matrix = (SparseMatrixCSC) PageRank.matrix_c;

        public CSCThreadMain (
            /*String file, SparseMatrix matrix,*/
            int id,
            double delta,
            double tol,
            double d,
            double x[],
            double y[],
            int outdeg[],
            int num_vertices,
            int num_threads
        ) {
            //matrix = new SparseMatrixCSC(file);
            //this.matrix = (SparseMatrixCSC) matrix;
            this.tol = tol;
            this.delta = delta;
            this.id = id;
            this.outdeg = outdeg;
            this.d = d;
            this.x = x;
            this.y = y;
            this.num_threads = num_threads;
            this.num_vertices = num_vertices;
        }

        @Override
        public void run() {
            int n = num_vertices;
            //synchronized (matrix) {
            while(true){
                try {
                    PageRank.barrier.await();

                    if (PageRank.flag == true) {
                        break;
                    }
                    //double a, double[] in, double[] out, int outdeg[], int startPos, int endPos
                    PageRank.matrix_c.iterateConcurrent(d, x, y, outdeg, iterateCalcIn(id, n, num_threads), iterateCalcOut(id, n, num_threads));

                    PageRank.barrier.await();

                    // Calculate residual error
                    delta = normdiff( x, y, n);

                    // Rescale to unit length and swap x[] and y[]
                    w = 1.0 / sum( y, n );
                    for( int i=0; i < n; ++i ) {
                        x[i] = y[i] * w;
                        y[i] = 0.;
                    }

                    if (delta > tol) {
                        PageRank.flag = true;
                    }
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
            //}
        }
    }

    static class CSCThreadOther extends Thread {
        int id;
        double tol;
        double delta;
        //Boolean flag = false;
        CyclicBarrier barrier = new CyclicBarrier(1);
        int[] outdeg;
        double d;
        double x[];
        double y[];
        int num_threads;
        int num_vertices;
        //SparseMatrixCSC matrix = (SparseMatrixCSC) PageRank.matrix_c;

        public CSCThreadOther (
            /*String file,SparseMatrix matrix,*/
            int id,
            double delta,
            double tol,
            double d,
            double x[],
            double y[],
            int outdeg[],
            int num_vertices,
            int num_threads
        ) {
            //matrix = new SparseMatrixCSC(file);
            //this.matrix = (SparseMatrixCSC) matrix;
            this.tol = tol;
            this.delta = delta;
            this.id = id;
            this.outdeg = outdeg;
            this.d = d;
            this.x = x;
            this.y = y;
            this.num_threads = num_threads;
            this.num_vertices = num_vertices;
        }

        @Override
        public void run() {
            int n = num_vertices;

            //synchronized(matrix) {
            while(true){
                try {
                    //Thread.sleep(200);
                    PageRank.barrier.await();

                    if (PageRank.flag == true) {
                        break;
                    }
                    PageRank.matrix_c.iterateConcurrent(d, x, y, outdeg, iterateCalcIn(id, n, num_threads), iterateCalcOut(id, n, num_threads));

                    PageRank.barrier.await();
                } catch (Exception ex) {
                    System.out.println(ex);
                }
            }
            //}
        }
    }



    //Calculate where a thread will begin its iteration
    //n = num_vertices, T = num_threads
    static int iterateCalcIn(int id, int n, int T) {
        int iterateNo = (id * (n*T));
        return iterateNo;
    }

    //Calculate where a thread will end its iteration
    static int iterateCalcOut(int id, int n, int T) {
        int iterateNo = (id +1) * (n/T);
        return iterateNo;
    }


}
