## ORKUT


### Build

We are using Maven so do:

```
mvn clean install
```


### Run

To run outside of Maven:

```
java assignment2.PageRank \
~/Downloads/orkut/orkut_undir.coo \
~/Downloads/orkut/orkut_undir.csc \
~/Downloads/orkut/orkut_undir.csc-csr \
10 \
~/Downloads/orkut/outputfile.txt
```

To run using the exec plugin:

```
mvn exec:java
```

N.B. You need to give Maven around 2Gb so set MAVEN_OPTS:

```
export MAVEN_OPTS="-Dmaven.artifact.threads=10 -Xms512m -Xmx2048m"
```

